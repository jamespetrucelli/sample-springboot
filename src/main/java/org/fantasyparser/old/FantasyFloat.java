
package org.fantasyparser.old;

public class FantasyFloat {
    String teamName;
    float floatValue;
    
    

    public FantasyFloat(String teamName, float floatValue){
        this.teamName = teamName;
        this.floatValue = floatValue;
    }
    /**
     * @return Returns the doubleValue.
     */
    public synchronized float getDoubleValue() {
        return floatValue;
    }
    /**
     * @param doubleValue The doubleValue to set.
     */
    public synchronized void setDoubleValue(float doubleValue) {
        this.floatValue = floatValue;
    }
    /**
     * @return Returns the teamName.
     */
    public synchronized String getTeamName() {
        return teamName;
    }
    /**
     * @param teamName The teamName to set.
     */
    public synchronized void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
