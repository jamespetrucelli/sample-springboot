
package org.fantasyparser.old;

public class FantasyInt {
    String teamName;
    int intValue;
    
    

    public FantasyInt(String teamName, int intValue){
        this.teamName = teamName;
        this.intValue = intValue;
    }
    /**
     * @return Returns the doubleValue.
     */
    public synchronized int getIntValue() {
        return intValue;
    }
    /**
     * @param doubleValue The doubleValue to set.
     */
    public synchronized void setIntValue(int intValue) {
        this.intValue = intValue;
    }
    /**
     * @return Returns the teamName.
     */
    public synchronized String getTeamName() {
        return teamName;
    }
    /**
     * @param teamName The teamName to set.
     */
    public synchronized void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
