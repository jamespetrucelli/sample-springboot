/**
 * 
 */
package org.fantasyparser.old;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author jlpetru
 *
 */
public class BaseballFantasyTeamDto {
	private String teamName;
	private Map <String,String> teamProperties;
	
	public BaseballFantasyTeamDto(){
		teamProperties = new HashMap<String,String>();
	}

	/**
	 * @return the teamName
	 */
	public String getTeamName() {
		return teamName;
	}

	/**
	 * @return the teamProperties
	 */
	public Map<String, String> getTeamProperties() {
		return teamProperties;
	}

	/**
	 * @param teamName the teamName to set
	 */
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	/**
	 * @param teamProperties the teamProperties to set
	 */
	public void setTeamProperties(Map<String, String> teamProperties) {
		this.teamProperties = teamProperties;
	}
	
	private int runs;

    private int homeRuns;

    private int RBIS;

    private int strikeOutsBatting;

    private int stolenBaseNet;

    private float OBP;

    private int tb;

    private int strikeOutsPitching;

    private int qualityStarts;

    private int wins;

    private int losses;

    private int saves;

    private float ERA;

    private float WHIP;

    private double runsPTS;

    private double homeRunsPTS;

    private double rbisPTS;

    private double strikeOutsBattingPTS;

    private double stolenBaseNetPTS;

    private double obpPTS;

    private double slgPTS;

    private double strikeOutsPitchingPTS;

    private double qualityStartsPTS;

    private double winsPTS;

    private double lossesPTS;

    private double savesPTS;

    private double eraPTS;

    private double whipPTS;
    
    private double tbPTS;
    
    

    /**
     * @return Returns the tBPTS.
     */
    public synchronized double getTBPTS() {
        return tbPTS;
    }
    private double totalPoints =0;

    

    public String toString() {
        StringBuffer sb = new StringBuffer();
        Method[] ms = BaseballFantasyTeamDto.class.getMethods();

        for (int i = 0; i < ms.length; i++) {
            Method m = ms[i];
            if ((m.getName().indexOf("getTeamName") >= 0) ||(m.getName().indexOf("getTotalPoints") >= 0) ||
                    (m.getName().indexOf("get") >= 0 && m.getName().indexOf("PTS") >= 0)) {
                

                
                String tagName = m.getName().substring(3, m.getName().length());
                if (tagName.equalsIgnoreCase("class"))
                    continue;

                try {

                    if(m.getName().indexOf("getTeamName") >=0){
                        String response = (String)m.invoke(this, null);
                        
                        if(m.getName().indexOf("getTeamName") >=0){
                            
                            while(response.indexOf(",") >=0){
                                String response1 = response.substring(0,response.indexOf(","));
                                String response2 = response1 + "(Comma)"+response.substring(response.indexOf(",") +1);
                                response = response2;
                            }
                            //response = response.replaceAll(",","(Fuck you Comma)");
                            
                            response = response +",";
                        sb.append(response);    
                        }
                    }else
                        sb.append(m.invoke(this, null) + ",");
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                
            }

        }

        return sb.toString().substring(0,sb.length() -1);
    }

	public int getRuns() {
		return runs;
	}

	public void setRuns(int runs) {
		this.runs = runs;
	}

	public int getHomeRuns() {
		return homeRuns;
	}

	public void setHomeRuns(int homeRuns) {
		this.homeRuns = homeRuns;
	}

	public int getRBIS() {
		return RBIS;
	}

	public void setRBIS(int rBIS) {
		RBIS = rBIS;
	}

	public int getStrikeOutsBatting() {
		return strikeOutsBatting;
	}

	public void setStrikeOutsBatting(int strikeOutsBatting) {
		this.strikeOutsBatting = strikeOutsBatting;
	}

	public int getStolenBaseNet() {
		return stolenBaseNet;
	}

	public void setStolenBaseNet(int stolenBaseNet) {
		this.stolenBaseNet = stolenBaseNet;
	}

	public float getOBP() {
		return OBP;
	}

	public void setOBP(float oBP) {
		OBP = oBP;
	}

	public int getTb() {
		return tb;
	}

	public void setTb(int tb) {
		this.tb = tb;
	}

	public int getStrikeOutsPitching() {
		return strikeOutsPitching;
	}

	public void setStrikeOutsPitching(int strikeOutsPitching) {
		this.strikeOutsPitching = strikeOutsPitching;
	}

	public int getQualityStarts() {
		return qualityStarts;
	}

	public void setQualityStarts(int qualityStarts) {
		this.qualityStarts = qualityStarts;
	}

	public int getWins() {
		return wins;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public int getLosses() {
		return losses;
	}

	public void setLosses(int losses) {
		this.losses = losses;
	}

	public int getSaves() {
		return saves;
	}

	public void setSaves(int saves) {
		this.saves = saves;
	}

	public float getERA() {
		return ERA;
	}

	public void setERA(float eRA) {
		ERA = eRA;
	}

	public float getWHIP() {
		return WHIP;
	}

	public void setWHIP(float wHIP) {
		WHIP = wHIP;
	}

	public double getRunsPTS() {
		return runsPTS;
	}

	public void setRunsPTS(Double runsPTS) {
		this.runsPTS = runsPTS;
	}

	public double getHomeRunsPTS() {
		return homeRunsPTS;
	}

	public void setHomeRunsPTS(Double homeRunsPTS) {
		this.homeRunsPTS = homeRunsPTS;
	}

	public double getRbisPTS() {
		return rbisPTS;
	}

	public void setRbisPTS(Double rbisPTS) {
		this.rbisPTS = rbisPTS;
	}

	public double getStrikeOutsBattingPTS() {
		return strikeOutsBattingPTS;
	}

	public void setStrikeOutsBattingPTS(Double strikeOutsBattingPTS) {
		this.strikeOutsBattingPTS = strikeOutsBattingPTS;
	}

	public double getStolenBaseNetPTS() {
		return stolenBaseNetPTS;
	}

	public void setStolenBaseNetPTS(Double stolenBaseNetPTS) {
		this.stolenBaseNetPTS = stolenBaseNetPTS;
	}

	public double getObpPTS() {
		return obpPTS;
	}

	public void setObpPTS(Double obpPTS) {
		this.obpPTS = obpPTS;
	}

	public double getSlgPTS() {
		return slgPTS;
	}

	public void setSlgPTS(Double slgPTS) {
		this.slgPTS = slgPTS;
	}

	public double getStrikeOutsPitchingPTS() {
		return strikeOutsPitchingPTS;
	}

	public void setStrikeOutsPitchingPTS(Double strikeOutsPitchingPTS) {
		this.strikeOutsPitchingPTS = strikeOutsPitchingPTS;
	}

	public double getQualityStartsPTS() {
		return qualityStartsPTS;
	}

	public void setQualityStartsPTS(Double qualityStartsPTS) {
		this.qualityStartsPTS = qualityStartsPTS;
	}

	public double getWinsPTS() {
		return winsPTS;
	}

	public void setWinsPTS(Double winsPTS) {
		this.winsPTS = winsPTS;
	}

	public double getLossesPTS() {
		return lossesPTS;
	}

	public void setLossesPTS(Double lossesPTS) {
		this.lossesPTS = lossesPTS;
	}

	public double getSavesPTS() {
		return savesPTS;
	}

	public void setSavesPTS(Double savesPTS) {
		this.savesPTS = savesPTS;
	}

	public double getEraPTS() {
		return eraPTS;
	}

	public void setEraPTS(Double eraPTS) {
		this.eraPTS = eraPTS;
	}

	public double getWhipPTS() {
		return whipPTS;
	}

	public void setWhipPTS(Double whipPTS) {
		this.whipPTS = whipPTS;
	}

	public double getTbPTS() {
		return tbPTS;
	}

	public void setTbPTS(Double tbPTS) {
		this.tbPTS = tbPTS;
	}

	public double getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(Double totalPoints) {
		this.totalPoints = totalPoints;
	}

}
