/**
 * 
 */
package org.fantasyparser.old;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author jlpetru
 *
 */
public class HtmlParser {
	private String urlString;
	private static final int LEAGUE_SIZE = 10;
	public HtmlParser(String url){
		this.urlString = url;
		
	}
	
	
	public static void main(String[] args) throws Exception {
        List<BaseballFantasyTeamDto> dtoList = new ArrayList<BaseballFantasyTeamDto>();
        for(BaseballFantasyTeamDto dto : HtmlParser.getDtoList()){
        	System.out.print(dto.getTeamName());
        	System.out.println(dto.getTotalPoints());
//        	System.out.print();
//        	System.out.print();
//        	System.out.print();
//        	System.out.print();
//        	System.out.print();
//        	System.out.print();
//        	System.out.print();
//        	System.out.print();
//        	System.out.print();
//        	System.out.print();
//        	System.out.print();
        	
        }
	}
	
	public static BaseballFantasyTeamDto[] getDtoList() throws SecurityException, NoSuchMethodException{
		List<BaseballFantasyTeamDto>teamList = null;
		Map<String,BaseballFantasyTeamDto> teamMap = new HashMap<String,BaseballFantasyTeamDto>();
        FantasyFloat[] era = new FantasyFloat[LEAGUE_SIZE];
        FantasyFloat[] whip = new FantasyFloat[LEAGUE_SIZE];
        FantasyFloat[] obp = new FantasyFloat[LEAGUE_SIZE];
        FantasyInt[] tb = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] runs = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] hrs = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] rbis = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] kwb = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] sbn = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] kwp = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] qs = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] w = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] ls = new FantasyInt[LEAGUE_SIZE];
        FantasyInt[] svs = new FantasyInt[LEAGUE_SIZE];
		HtmlParser parser = new HtmlParser("http://games.espn.go.com/flb/standings?leagueId=10050");
		
		try {
			teamList = parser.parseStats();
		} catch (FantasyException e) {
			System.out.println(e.getCode());
		}
		
		//TODO cleanup below
        for(int x =0; x < teamList.size(); x++){
        	BaseballFantasyTeamDto dto = teamList.get(x);
            teamMap.put(dto.getTeamName(),dto);
            
            FantasyFloat fl = new FantasyFloat(dto.getTeamName(),dto.getERA());
            era[x] = fl;
            fl = new FantasyFloat(dto.getTeamName(),dto.getWHIP());
            whip[x] = fl;
             fl = new FantasyFloat(dto.getTeamName(),dto.getOBP());
            obp[x] = fl;

            
            FantasyInt fi = new FantasyInt(dto.getTeamName(),dto.getRuns());
            runs[x] = fi;
            fi = new FantasyInt(dto.getTeamName(),dto.getTb());
            tb[x] = fi;
            fi = new FantasyInt(dto.getTeamName(),dto.getHomeRuns());
            hrs[x] = fi;
             fi = new FantasyInt(dto.getTeamName(),dto.getRBIS());
            rbis[x] = fi;
             fi = new FantasyInt(dto.getTeamName(),dto.getStrikeOutsBatting());
            kwb[x] = fi;
             fi = new FantasyInt(dto.getTeamName(),dto.getStolenBaseNet());
            sbn[x] = fi;
             fi = new FantasyInt(dto.getTeamName(),dto.getStrikeOutsPitching());
            kwp[x] = fi;
             fi = new FantasyInt(dto.getTeamName(),dto.getWins());
            w[x] = fi;
            fi = new FantasyInt(dto.getTeamName(),dto.getQualityStarts());
            qs[x] = fi;
             fi = new FantasyInt(dto.getTeamName(),dto.getLosses());
            ls[x] = fi;
             fi = new FantasyInt(dto.getTeamName(),dto.getSaves());
            svs[x] = fi;
            
            
        }

        
       //System.out.println("STAT = runs"); 
       awardIntPoints(runs,false,teamMap,"RunsPTS");
       
       //System.out.println("STAT = hrs"); 
       awardIntPoints(hrs,false,teamMap,"HomeRunsPTS");
       
       //System.out.println("STAT = rbis"); 
       awardIntPoints(rbis,false,teamMap,"RbisPTS");
       
       //System.out.println("STAT = kwb"); 
       awardIntPoints(kwb,true,teamMap,"StrikeOutsBattingPTS");
       
       //System.out.println("STAT = sbn"); 
       awardIntPoints(sbn,false,teamMap,"StolenBaseNetPTS");
       
       //System.out.println("STAT = kwp"); 
       awardIntPoints(kwp,false,teamMap,"StrikeOutsPitchingPTS");
       
       ////System.out.println("STAT = qs"); 
       awardIntPoints(qs,false,teamMap,"QualityStartsPTS");
       
       ////System.out.println("STAT = w"); 
       awardIntPoints(w,false,teamMap,"WinsPTS");
       
       ////System.out.println("STAT = ls"); 
       awardIntPoints(ls,true,teamMap,"LossesPTS");
       
       ////System.out.println("STAT = svs"); 
       awardIntPoints(svs,false,teamMap,"SavesPTS");
       
       ////System.out.println("STAT = era"); 
       awardFloatPoints(era,true,teamMap,"EraPTS");
       
       ////System.out.println("STAT =whip "); 
       awardFloatPoints(whip,true,teamMap,"WhipPTS");
       
       ////System.out.println("STAT = obp"); 
       awardFloatPoints(obp,false,teamMap,"ObpPTS");
       
       ////System.out.println("STAT = slg"); 
       awardIntPoints(tb,false,teamMap,"TbPTS");
       
       
       Iterator i = teamMap.keySet().iterator();
       BaseballFantasyTeamDto[] dtos = new BaseballFantasyTeamDto[LEAGUE_SIZE];
       int y =0;
       while(i.hasNext()){
           String key = (String)i.next();
          
           BaseballFantasyTeamDto dto = (BaseballFantasyTeamDto)teamMap.get(key);
           dtos[y++] = dto;

           
       }
       
       selectionSortTotal(dtos);
       return dtos;
	}
	
	
	
	
	
    public static void selectionSortTotal(BaseballFantasyTeamDto[] x) {
        
        for (int i=0; i<x.length-1; i++) {
            for (int j=i+1; j<x.length; j++) {
                if (x[i].getTotalPoints() < x[j].getTotalPoints()) {
                    //... Exchange elements
                    BaseballFantasyTeamDto temp = x[i];
                    x[i] = x[j];
                    x[j] = temp;
                }
            }
        }
    }
	
	public List<BaseballFantasyTeamDto> parseStats() throws FantasyException{
		List<BaseballFantasyTeamDto>teamList = new ArrayList<BaseballFantasyTeamDto>();
		String html = null;
		String htmlStatTable = null;
		
		//Get the whole HTML page
		try {
			html = retrieveStatPage();
		} catch (IOException e) {
			e.printStackTrace();
			throw (new FantasyException("Error Retrieving Stat Page.",e));
		}
		//Retrieve the HTML for just this stat section
		htmlStatTable = retrieveStatSection(html);
		
		//Parse the team list and stats
		teamList = parseTeamRecord(htmlStatTable);
		
		
		return teamList;
	}
	
	private String retrieveStatSection(String htmlPage){
        StringBuilder tableSection = new StringBuilder();
        String tsStart = htmlPage.substring(htmlPage.indexOf("tableBody sortableRow"));
        String tsFull = tsStart.substring(0,tsStart.indexOf("</table>"));
        tableSection.append(tsFull);
		return tableSection.toString();
	}
	
	private List<BaseballFantasyTeamDto> parseTeamRecord(String html){
		List<BaseballFantasyTeamDto> dtoList = new ArrayList<BaseballFantasyTeamDto>();
		String[] splitArray = html.split("</tr>");
		for(int i = 0; i < splitArray.length -1;i++){
			BaseballFantasyTeamDto dto = new BaseballFantasyTeamDto();
			splitArray[i] = removeTeamName(splitArray[i], dto);
			//System.out.println(dto.getTeamName());
			parseStats(splitArray[i],dto);
			dtoList.add(dto);
		}
		
		return dtoList;
	}
	
	private void mapStat(String statNum,String statValue,BaseballFantasyTeamDto dto){
		if(statNum.equals("20")){
			dto.setRuns(Integer.parseInt(statValue));
		}else if(statNum.equals("5")){
			dto.setHomeRuns(Integer.parseInt(statValue));
		}else if(statNum.equals("8")){
			dto.setTb(Integer.parseInt(statValue));
		}else if(statNum.equals("21")){
			dto.setRBIS(Integer.parseInt(statValue));
		}else if(statNum.equals("27")){
			dto.setStrikeOutsBatting(Integer.parseInt(statValue));
		}else if(statNum.equals("25")){
			dto.setStolenBaseNet(Integer.parseInt(statValue));
		}else if(statNum.equals("17")){
			dto.setOBP(Float.parseFloat(statValue));
		}else if(statNum.equals("48")){
			dto.setStrikeOutsPitching(Integer.parseInt(statValue));
		}else if(statNum.equals("63")){
			dto.setQualityStarts(Integer.parseInt(statValue));
		}else if(statNum.equals("53")){
			dto.setWins(Integer.parseInt(statValue));
		}else if(statNum.equals("54")){
			dto.setLosses(Integer.parseInt(statValue));
		}else if(statNum.equals("57")){
			dto.setSaves(Integer.parseInt(statValue));
		}else if(statNum.equals("47")){
			dto.setERA(Float.parseFloat(statValue));
		}else if(statNum.equals("41")){
			dto.setWHIP(Float.parseFloat(statValue));
		}
		
	}
	
	private BaseballFantasyTeamDto parseStats(String html, BaseballFantasyTeamDto dto){
		String[] splitArray = html.split("</td>");
		for(int i = 0; i < splitArray.length -1; i++){
			if(splitArray[i].indexOf("sectionLeadingSpacer") >= 0 || splitArray[i].indexOf("sortableLast") >= 0)
				continue;
			String splitLine = splitArray[i].substring(splitArray[i].indexOf("sortableStat") +12);
			String statNumber = splitLine.substring(0,splitLine.indexOf("\""));
			String statValue = splitLine.substring(splitLine.indexOf(">") +1);
			dto.getTeamProperties().put(statNumber, statValue);
			mapStat(statNumber,statValue,dto);
		} 
		
		return dto;
	}
	
	private String removeTeamName(String html,BaseballFantasyTeamDto dto){
		int indexStart = html.indexOf("<a title=\"") + 10;
		html = html.substring(indexStart);
		dto.setTeamName(html.substring(0,html.indexOf("\" href")));
		//Get to the end
		html = html.substring(html.indexOf("</td>") +5);
		//Remove the blank line
		html = html.substring(html.indexOf("</td>") +5);
		return html;
	}
	
	private String retrieveStatPage() throws IOException{
	    URL url;
		url = new URL(urlString);
		URLConnection urlConnection = url.openConnection();

		urlConnection.setAllowUserInteraction(false);

		InputStream urlStream = url.openStream();

		
		byte b[] = new byte[1000];
		int numRead = urlStream.read(b);
		String content = new String(b, 0, numRead);
		while (numRead != -1) {
		    numRead = urlStream.read(b);
		    if (numRead != -1) {
			String newContent = new String(b, 0, numRead);
			content += newContent;
		    }
		}

		urlStream.close();
		return content;
	}
	
	
	
	
	
    
    public static int awardIntPoints(FantasyInt[] fl, boolean dsc, Map teamMap,String methodName) throws SecurityException, NoSuchMethodException{
        boolean finalIsTie = false;
        if (!dsc) {
            selectionSortInt(fl,false);
            int counter = LEAGUE_SIZE;
            int decrement = 1;
            for (int i = 0; i < fl.length - 1; i++) {
                if (fl[i].getIntValue() > fl[i + 1].getIntValue()) {
                	BaseballFantasyTeamDto dto = (BaseballFantasyTeamDto) teamMap.get(fl[i]
                            .getTeamName());
                    dto.setTotalPoints(dto.getTotalPoints() + counter);
                    changeToRewardPoint(dto,methodName, new Double(counter));
                    //System.out.println(fl[i].getTeamName() + " is getting " + counter);
                    teamMap.put(fl[i].getTeamName(),dto);
                    counter = counter - decrement;
                    decrement = 1;

                } else if (fl[i].getIntValue() == fl[i+1].getIntValue()) {
                    List tempTeams = new ArrayList();
                    
                    for(int l = i; l < fl.length -1; l++){
                        if(fl[l].getIntValue() == fl[l+1].getIntValue()){
                            tempTeams.add(teamMap.get(fl[l].getTeamName()));
                            i++;
                            if((l + 2 <= fl.length -1) && !(fl[l+1].getIntValue() == fl[l+2].getIntValue())){
                                tempTeams.add(teamMap.get(fl[l +1].getTeamName()));
    
                            }else if(l +2 > fl.length -1){
                                tempTeams.add(teamMap.get(fl[l +1].getTeamName()));
                  
                                finalIsTie = true;
                                break;
                            }
                        }else{
                            break;
                        }
                    }
                    
                    double ptsToAward = 0;
                    //System.out.println("Size of the list " + tempTeams.size());
                    for(int l =0; l < tempTeams.size(); l++){
                        //System.out.println("!!!!!! " + ((BaseballFantasyTeamDto)tempTeams.get(l)).getTeamName());
                        ptsToAward = ptsToAward + (counter - l);
                        
                    }
                    
                    ptsToAward = ptsToAward/tempTeams.size();
                    
                    for(int l =0; l < tempTeams.size(); l++){
                        BaseballFantasyTeamDto dto = ((BaseballFantasyTeamDto)tempTeams.get(l));
                        dto.setTotalPoints(dto.getTotalPoints() + ptsToAward);
                        //System.out.println(dto.getTeamName() + " is getting " + ptsToAward);
                        changeToRewardPoint(dto,methodName,new Double(ptsToAward));
                        teamMap.put(dto.getTeamName(),dto);
                        
                    }
                    

                    decrement = decrement + tempTeams.size() -1;
                    counter = counter - decrement;
                    decrement = 1;
                }


            }
            
            if(!finalIsTie){
            BaseballFantasyTeamDto dto = (BaseballFantasyTeamDto) teamMap.get(fl[fl.length - 1]
                                                         .getTeamName());
            dto.setTotalPoints(dto.getTotalPoints() + counter);
            changeToRewardPoint(dto,methodName, new Double(counter));
            teamMap.put(fl[fl.length - 1].getTeamName(),dto);
            //System.out.println(fl[fl.length - 1].getTeamName() + " is getting " + counter);
            
            }
        }else{
            selectionSortInt(fl,true);
            int counter = LEAGUE_SIZE;
            int decrement = 1;
            for (int i = 0; i < fl.length - 1; i++) {
                if (fl[i].getIntValue() < fl[i + 1].getIntValue()) {
                    BaseballFantasyTeamDto dto = (BaseballFantasyTeamDto) teamMap.get(fl[i]
                            .getTeamName());
                    dto.setTotalPoints(dto.getTotalPoints() + counter);
                    changeToRewardPoint(dto,methodName, new Double(counter));
                    teamMap.put(fl[i]
                                   .getTeamName(),dto);
                    //System.out.println(fl[i].getTeamName() + " is getting " + counter);
                    counter = counter - decrement;
                    decrement = 1;

                } else if (fl[i].getIntValue() == fl[i+1].getIntValue()) {
                    List tempTeams = new ArrayList();
                    
                    for(int l = i; l < fl.length -1; l++){
                        if(fl[l].getIntValue() == fl[l+1].getIntValue()){
                            tempTeams.add(teamMap.get(fl[l].getTeamName()));
                            i++;
                            if((l + 2 <=fl.length -1) && !(fl[l+1].getIntValue() == fl[l+2].getIntValue())){
                                
                                tempTeams.add(teamMap.get(fl[l +1].getTeamName()));
         
                            }else if(l +2 > fl.length -1){
                                tempTeams.add(teamMap.get(fl[l +1].getTeamName()));
                   
                                finalIsTie = true;
                                break;
                            }
                        }else{
                            break;
                        }
                    }
                    
                    double ptsToAward = 0;
                    //System.out.println("Size of the list " + tempTeams.size());
                    for(int l =0; l < tempTeams.size(); l++){
                        //System.out.println("!!!!!! " + ((BaseballFantasyTeamDto)tempTeams.get(l)).getTeamName());
                        ptsToAward = ptsToAward + (counter - l);
                        
                    }
                    
                    ptsToAward = ptsToAward/tempTeams.size();
                    
                    for(int l =0; l < tempTeams.size(); l++){
                        BaseballFantasyTeamDto dto = ((BaseballFantasyTeamDto)tempTeams.get(l));
                        dto.setTotalPoints(dto.getTotalPoints() + ptsToAward);
                        //System.out.println(dto.getTeamName() + " is getting " + ptsToAward);
                        changeToRewardPoint(dto,methodName, new Double(ptsToAward));
                        teamMap.put(dto.getTeamName(),dto);
                        
                    }
                    

                    decrement = decrement + tempTeams.size() -1;
                    counter = counter - decrement;
                    decrement = 1;
                }


            }
            
            if(!finalIsTie){
                BaseballFantasyTeamDto dto = (BaseballFantasyTeamDto) teamMap.get(fl[fl.length - 1]
                                                             .getTeamName());
                dto.setTotalPoints(dto.getTotalPoints() + counter);
                changeToRewardPoint(dto,methodName, new Double(counter));
                teamMap.put(fl[fl.length - 1].getTeamName(),dto);
                //System.out.println(fl[fl.length - 1].getTeamName() + " is getting " + counter);
            
            
            }
        }
        return 0;
    }

    
    private static void populateTableValues(Object[][] values, BaseballFantasyTeamDto[] dtos){
        for(int i = 0; i < dtos.length; i ++){
            BaseballFantasyTeamDto dto = dtos[i];
            values[i][0] = dto.getTeamName();
            values[i][1] = new Double(dto.getRunsPTS());
            values[i][2] = new Double(dto.getHomeRunsPTS());
            values[i][3] = new Double(dto.getTBPTS());
            values[i][4] = new Double(dto.getRbisPTS());
            values[i][5] = new Double(dto.getStrikeOutsBattingPTS());
            values[i][6] = new Double(dto.getStolenBaseNetPTS());
            values[i][7] = new Double(dto.getObpPTS());
           
            values[i][8] = new Double(dto.getStrikeOutsPitchingPTS());
            values[i][10] = new Double(dto.getWinsPTS());
            values[i][9] = new Double(dto.getQualityStartsPTS());
            values[i][11] = new Double(dto.getLossesPTS());
            values[i][12] = new Double(dto.getSavesPTS());
            values[i][13] = new Double(dto.getEraPTS());
            values[i][14] = new Double(dto.getWhipPTS());
            values[i][15] = new Double(dto.getTotalPoints());
            
        }
        
        
    }
    
    private static void changeToRewardPoint(BaseballFantasyTeamDto dto, String methodName,Double value){
        

 
        try{
            Class[] param = {value.getClass()};
            Object[] obs = {value};
            Method method = dto.getClass().getMethod("set" + methodName,param);
            method.invoke(dto, obs);
        }catch(Exception e){
            e.printStackTrace();
        }

        
    }
    
    
    public static int awardFloatPoints(FantasyFloat[] fl, boolean dsc, Map teamMap, String methodName) throws SecurityException, NoSuchMethodException{
       boolean finalIsTie = false;
        if (dsc) {
            selectionSortFloat(fl,true);
            int counter = LEAGUE_SIZE;
            int decrement = 1;
            for (int i = 0; i < fl.length - 1; i++) {
                if (fl[i].getDoubleValue() < fl[i + 1].getDoubleValue()) {
                    BaseballFantasyTeamDto dto = (BaseballFantasyTeamDto) teamMap.get(fl[i]
                            .getTeamName());
                    dto.setTotalPoints(dto.getTotalPoints() + counter);
                    changeToRewardPoint(dto,methodName, new Double(counter));
                    teamMap.put(fl[i]
                                   .getTeamName(),dto);
                    //System.out.println(fl[i].getTeamName() + " is getting " + counter);
                    counter = counter - decrement;
                    decrement = 1;

                } else if (fl[i].getDoubleValue() == fl[i].getDoubleValue()) {
                    List tempTeams = new ArrayList();
                    
                    for(int l = i; l < fl.length -1; l++){
                        if(fl[l].getDoubleValue() == fl[l+1].getDoubleValue()){
                            tempTeams.add(teamMap.get(fl[l].getTeamName()));
                            i++;
                            if((l + 2 <= fl.length -1) && !(fl[l+1].getDoubleValue() == fl[l+2].getDoubleValue())){
                                tempTeams.add(teamMap.get(fl[l +1].getTeamName()));
                            }else if(l +2 > fl.length -1){
                                tempTeams.add(teamMap.get(fl[l +1].getTeamName()));
                                finalIsTie = true;
                                break;
                            }
                        }else{
                            break;
                        }
                    }
                    
                    double ptsToAward = 0;
                    //System.out.println("Size of the list " + tempTeams.size());
                    for(int l =0; l < tempTeams.size(); l++){
                        //System.out.println("!!!!!! " + ((BaseballFantasyTeamDto)tempTeams.get(l)).getTeamName());
                        ptsToAward = ptsToAward + (counter - l);
                        
                    }
                    
                    ptsToAward = ptsToAward/tempTeams.size();
                    
                    for(int l =0; l < tempTeams.size(); l++){
                        BaseballFantasyTeamDto dto = ((BaseballFantasyTeamDto)tempTeams.get(l));
                        dto.setTotalPoints(dto.getTotalPoints() + ptsToAward);
                        //System.out.println(dto.getTeamName() + " is getting " + ptsToAward);
                        changeToRewardPoint(dto,methodName,new Double(ptsToAward));
                        teamMap.put(dto.getTeamName(),dto);
                        
                    }
                    

                    decrement = decrement + tempTeams.size() -1;
                    counter = counter - decrement;
                    decrement = 1;

                }


            }
            if(!finalIsTie){
                BaseballFantasyTeamDto dto = (BaseballFantasyTeamDto) teamMap.get(fl[fl.length - 1]
                                                             .getTeamName());
                dto.setTotalPoints(dto.getTotalPoints() + counter);
                changeToRewardPoint(dto,methodName, new Double(counter));
                teamMap.put(fl[fl.length - 1].getTeamName(),dto);
                //System.out.println(fl[fl.length - 1].getTeamName() + " is getting " + counter);
            
            
            }
        }else{
        
                selectionSortFloat(fl,false);
                int counter = LEAGUE_SIZE;
                int decrement = 1;
                for (int i = 0; i < fl.length - 1; i++) {
                    if (fl[i].getDoubleValue() > fl[i + 1].getDoubleValue()) {
                        BaseballFantasyTeamDto dto = (BaseballFantasyTeamDto) teamMap.get(fl[i]
                                .getTeamName());
                        dto.setTotalPoints(dto.getTotalPoints() + counter);
                        changeToRewardPoint(dto,methodName, new Double(counter));
                        teamMap.put(fl[i]
                                       .getTeamName(),dto);
                        //System.out.println(fl[i].getTeamName() + " is getting " + counter);
                        
                        counter = counter - decrement;
                        decrement = 1;

                    } else if (fl[i].getDoubleValue() == fl[i].getDoubleValue()) {
                        List tempTeams = new ArrayList();
                        
                        for(int l = i; l < fl.length -1; l++){
                            if(fl[l].getDoubleValue() == fl[l+1].getDoubleValue()){
                                tempTeams.add(teamMap.get(fl[l].getTeamName()));
                                i++;
                                if((l + 2 <= fl.length -1) && !(fl[l+1].getDoubleValue() == fl[l+2].getDoubleValue())){
                                    tempTeams.add(teamMap.get(fl[l +1].getTeamName()));
 
                                }else if(l +2 > fl.length -1){
                                    tempTeams.add(teamMap.get(fl[l +1].getTeamName()));
       
                                    finalIsTie = true;
                                    break;
                                }
                            }else{
                                break;
                            }
                        }
                        
                        double ptsToAward = 0;
                        //System.out.println("Size of the list " + tempTeams.size());
                        for(int l =0; l < tempTeams.size(); l++){
                            //System.out.println("!!!!!! " + ((BaseballFantasyTeamDto)tempTeams.get(l)).getTeamName());
                            ptsToAward = ptsToAward + (counter - l);
                            
                        }
                        
                        ptsToAward = ptsToAward/tempTeams.size();
                        
                        for(int l =0; l < tempTeams.size(); l++){
                            BaseballFantasyTeamDto dto = ((BaseballFantasyTeamDto)tempTeams.get(l));
                            dto.setTotalPoints(dto.getTotalPoints() + ptsToAward);
                            //System.out.println(dto.getTeamName() + " is getting " + ptsToAward);
                            changeToRewardPoint(dto,methodName,new Double(ptsToAward));
                            teamMap.put(dto.getTeamName(),dto);
                            
                        }
                        

                        decrement = decrement + tempTeams.size() -1;
                        counter = counter - decrement;
                        decrement = 1;

                    }


                }
                
                
                if(!finalIsTie){
                    BaseballFantasyTeamDto dto = (BaseballFantasyTeamDto) teamMap.get(fl[fl.length - 1]
                                                                 .getTeamName());
                    dto.setTotalPoints(dto.getTotalPoints() + counter);
                    changeToRewardPoint(dto,methodName, new Double(counter));
                    teamMap.put(fl[fl.length - 1].getTeamName(),dto);
                    //System.out.println(fl[fl.length - 1].getTeamName() + " is getting " + counter);
                
                
                }
            
        }
        return 0;
    }
    
    
    
    public static void selectionSortInt(FantasyInt[] x,boolean isAsc) {
        if(isAsc){
	        for (int i=0; i<x.length-1; i++) {
	            for (int j=i+1; j<x.length; j++) {
	                if (x[i].getIntValue() > x[j].getIntValue()) {
	                    //... Exchange elements
	                    FantasyInt temp = x[i];
	                    x[i] = x[j];
	                    x[j] = temp;
	                }
	            }
	        }
        }else{
            for (int i=0; i<x.length-1; i++) {
                for (int j=i+1; j<x.length; j++) {
                    if (x[i].getIntValue() < x[j].getIntValue()) {
                        //... Exchange elements
                        FantasyInt temp = x[i];
                        x[i] = x[j];
                        x[j] = temp;
                    }
                }
            }
        	
        }
    }
    
    public static void selectionSortFloat(FantasyFloat[] x,boolean isAsc) {
        if(isAsc){
	        for (int i=0; i<x.length-1; i++) {
	            for (int j=i+1; j<x.length; j++) {
	                if (x[i].getDoubleValue() > x[j].getDoubleValue()) {
	                    //... Exchange elements
	                    FantasyFloat temp = x[i];
	                    x[i] = x[j];
	                    x[j] = temp;
	                }
	            }
	        }
        }else{
            for (int i=0; i<x.length-1; i++) {
                for (int j=i+1; j<x.length; j++) {
                    if (x[i].getDoubleValue() < x[j].getDoubleValue()) {
                        //... Exchange elements
                        FantasyFloat temp = x[i];
                        x[i] = x[j];
                        x[j] = temp;
                    }
                }
            }
        	
        }
    }
}
	
    
    
  