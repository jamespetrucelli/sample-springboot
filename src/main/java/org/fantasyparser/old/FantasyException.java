/**
 * 
 */
package org.fantasyparser.old;


/**
 * @author jlpetru
 *
 */
public class FantasyException extends Exception{
	private String code;
	
    public FantasyException(String code, Throwable cause) {
        super(cause);
        this.code = code;
    }

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
    
    
	

}
