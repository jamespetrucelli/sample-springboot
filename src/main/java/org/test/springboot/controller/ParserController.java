package org.test.springboot.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.fantasyparser.old.BaseballFantasyTeamDto;
import org.fantasyparser.old.HtmlParser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.test.springboot.domain.FTGLightDto;

@RestController
public class ParserController {

	
    @RequestMapping("/ftgparser")
    public List<BaseballFantasyTeamDto> greeting() {
    
        try {
			return Arrays.asList(HtmlParser.getDtoList());
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return null;
		
    }
    
    
    @RequestMapping("/ftgparserlight")
    public List<FTGLightDto> ftglight() {
  
        try {
        	List<BaseballFantasyTeamDto> dtoList =   Arrays.asList(HtmlParser.getDtoList());
        	List<FTGLightDto> lightList = new ArrayList();
        	for(BaseballFantasyTeamDto dto : dtoList) {
        		FTGLightDto newDto = new FTGLightDto();
        		newDto.setTeamName(dto.getTeamName());
        		newDto.setTotalPoints(dto.getTotalPoints());
        		lightList.add(newDto);
        	}
        	
        	
			return lightList;
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return null;
		
    }
}
