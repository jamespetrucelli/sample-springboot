package org.test.springboot.controller;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.test.springboot.domain.User;

@RestController
public class UserController {

	

    
    
   
    private final AtomicInteger counter = new AtomicInteger();

    @RequestMapping("/user")
    public User greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new User(name,"Petrucelli",counter.incrementAndGet());
    }
    
    @RequestMapping("/userUpload")
    public User greeting(@RequestParam(value="firstName", defaultValue="jamie") String firstName,@RequestParam(value="lastName", defaultValue="pet") String lastName) {
        return new User(firstName,lastName,counter.incrementAndGet());
    }
    
    
}
