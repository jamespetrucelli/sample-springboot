$(document).ready( function () {
	 var table = $('#ftgPlayers').DataTable({
			"sAjaxSource": "/ftgparser",
			"sAjaxDataProp": "",
			"order": [[ 0, "asc" ]],
			"aoColumns": [
				  { "mData": "teamName" },
				  { "mData": "runsPTS" },
				  { "mData": "homeRunsPTS" },
				  { "mData": "tbPTS" },
				  { "mData": "rbisPTS" },
				  { "mData": "strikeOutsBattingPTS" },
				  { "mData": "stolenBaseNetPTS" },
				  { "mData": "obpPTS" },
				  { "mData": "strikeOutsPitchingPTS" },
				  { "mData": "qualityStartsPTS" },
				  { "mData": "winsPTS" },
				  { "mData": "lossesPTS" },
				  { "mData": "savesPTS" },
				  { "mData": "eraPTS" },
				  { "mData": "whipPTS" },
				  { "mData": "totalPoints" }
			]
	 })
});