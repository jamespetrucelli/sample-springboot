<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.ftgparser.org">
<head>
	<meta charset="utf-8" />
	<title>FTG Fantasy Baseball</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="css/jquery.dataTables.min.css">
	<script src="js/jquery.dataTables.min.js"></script>
	<script src="js/datatables.js"></script>
</head>

<body>
	<table id="ftgPlayers" class="display">
      
       <!-- Header Table -->
       <thead>
            <tr>
				<th>Name</th>
				<th>Runs</th>
				<th>HR</th>
				<th>TB</th>
				<th>RBI</th>
				<th>K</th>
				<th>SBN</th>
				<th>OBP</th>
				<th>K</th>
				<th>QS</th>
				<th>W</th>
				<th>L</th>
				<th>SV</th>
				<th>ERA</th>
				<th>WHIP</th>
                <th>Total Points<th>
            </tr>
        </thead>
      <!--   Footer Table 
        <tfoot>
            <tr>
            <t
				<th>Name</th>
				<th>Runs</th>
				<th>HR</th>
				<th>TB</th>
				<th>RBI</th>
				<th>K</th>
				<th>SBN</th>
				<th>OBP</th>
				<th>K</th>
				<th>QS</th>
				<th>W</th>
				<th>L</th>
				<th>SV</th>
				<th>ERA</th>
				<th>WHIP</th>
                <th>Total Points<th>
            </tr>
        </tfoot> -->
    </table>
    
</body>
</html>